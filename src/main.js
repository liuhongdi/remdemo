import { createApp } from 'vue'
import App from './App.vue'

//判断是否移动端的函数
const isMobileFunc = () => {
    let flag = navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i);
    if (flag === null) {
        return 0;
    } else {
        return 1;
    }
};

//设置html的字号大小，作为rem的计算依据
const setHtmlFontSize = () => {
    let designWidth = 750;
    const htmlDom = document.getElementsByTagName('html')[0];
    let htmlWidth = document.documentElement.clientWidth || document.body.clientWidth;
    if (isMobileFunc() === 1) {
        //移动端
        htmlDom.style.fontSize = `${htmlWidth / designWidth}px`;
    } else {
        //非移动端:
        //console.log('htmlWidth:'+htmlWidth);
        let dest = htmlWidth * 0.36;
        if (dest < 360) {
            dest = 360;
        }
        htmlDom.style.fontSize = `${dest / designWidth}px`;
    }
};

window.onresize = setHtmlFontSize;
setHtmlFontSize();

createApp(App).mount('#app')
